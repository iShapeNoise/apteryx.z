
## Apteryx.z Performances

---

### 16/05/20 >> Uroboros Festival 2020

![](Uroboros.jpg)

[On YouTube](https://www.youtube.com/watch?v=OJLQhpbdXBg)

[As FoxDot Leftover](file:///200516_uroboros_apteryx.z.py)

---

### 20/03/20 >> EulerRoomEquinox Festival 2020

![](EulerRoomEquinox.jpg)

[On YouTube](https://www.youtube.com/watch?v=Lp3tm-3VkYI&t=1004s)


[As FoxDot Leftover](file:///200516_uroboros_apteryx.z.py)
