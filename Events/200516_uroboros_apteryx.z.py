####<<<<<00~ APTERYXZ ~00>>>>>#####
Clock.bpm = var([25,170],[16,128])


bassl=P[0,var([6,5,4,3],8),var([2,1],32),var([7,6,5,4,3],64)]
notes=P[var([5,4,3,2,1],32),var([5,4,3,2],[64,32]),[5,4,4,4,6]]

var.inout=var([1,0,0,0,0,0,0,1],[128,64,64,12,8,64,64,128,128])
var.brk1=var([0,1,0,0,1,0,0,0],[128,64,64,128,64,64,128,128])
var.bld=var([0,0,1,0,0,1,0,0],[128,64,64,128,64,64,128,128])
var.drp=var([0,0,0,1,0,0,1,0],[128,64,64,128,64,64,128,128])
var.brk = var([1, 0], [31, 1, 28, 4])

xx>>marimba(P[0,(3,0),2,(4,6),5,4,3,4],dur=1/4,delay=2/3,amp=3,mix=0.25, room=0.75)

xy>>charm(P[2,1,2,5,6,7,5,3,4,5,6,7,8,9,0,1,2,3,4,5,6,5,2,8,5,6,0,1,7,2],dur=1/16,chop=var([2,0,2,1],2),amp=var([1.5,1],[16,4])).every(16,'reverse')

Master().rate = 1

#HALP# do you hear it as well
#ITHINK THIS IS THE END LMFAO 
# THE SERVER IS FUUU

# amazink
ss >> play('s', dur=0.25, bpm=1000, lpf=4700, lpr=0.5, amp=P[1, 0.5, 0.25, 0.75] * var([1, 0], [5, 2, 4, 2, 1]))

rs >> play('I', dur=[0.5, 0.25, 0.25], amp=1.25)

t5 >> play('T', dur=4, amp=2.5)

jj >> jbass(bpm=1000, lpf=expvar([500, 2000], [16, 0]), lpr=0.25, hpf=expvar([200, 800], 32), hpr=0.25, amp=expvar([1, 0], 1/3))

vv >> play('k', sample=P[var([1, 0, 2, 2], 64), var([0, 1], 8), 0, var([2, 1, 0, 0], 32)], dur=0.5, bpm=700, hpf=P[1, 0.75, [0.5, 0.25, 0.75, 1], [0.5, 0.75, 1, 1]] * 120, amp=P[[0.25, 0.5, 0.75, 1], [0, 0.5, 1, 1], 0.75, 1] * var([0.5, 0], [3, 1, 2, 4, 2, 4,5]))

kd >> play('X', dur=[1] * 8 + [0.5] * 4 + [0.25] * 8, sample=(1, 2), amp=1.5)


Master().lpf = 1500

Master().lpr = 0.5

# BUY (half a) BITCOIN
# internet owns us
# mind control

#i

oh >> play('-', room=1, mix=0.15, sample=var([1, 0], 128), dur=kk.dur, delay=0.5, amp=var([1, 0], [24, 8]) * var([0, 1], [32, 64, 64, 128]))
pl >> jbass(dur=0.25, sus=P[2, 2, var([2, 1], [128, 64]), 2], drive=P[0.15, 0.1, 0.05, 0], chop=P[4, 4, var([8, 4], [28, 4])], hpf=P[1, [0.85, 0.5, 0.75, 1], [0.5, 1], [0.75, 1, 0.75, 1.25]] * 800, hpr=0.25, amp=var([1, 0], [128, 64, 256, 128]))

m1 >> dab(drive=0.5, amp=2, dur=2)

d0 >> play('D', mix=0.35, echo=0.625, echotime=2, shape=0.5, bend=0.5, amp=0.15, drive=0.1, room=0.25, dur=4, hpf=P[800,1600,1000], hpr=0.35).every(4, 'stutter')

a0 >> play('A', mix=0.25, room=0.75, lpf=1000, amp=0.05, drive=0.25, shape=0.35, dur=2)

s7 >> bass(P[3,2,1].stutter([16,16,32])-3, drive=[0.2,0.1,0.3], amp=2*P[0.75,0.75,1,1], dur=0.25)  # <3
                                                        
s8 >> dbass(P[0,var([3,2,0],[12,[8,4,4]])], dur=0.25 *P[1,1,1,3], amp=1, sus=0.5, lpf=1000)

s9 >> blip([0,[[2,2,2,[2,2,4]],0]], dur=0.25, amp=linvar([0.5,1.4],[12]), oct=6, delay=0)

# Join our telegram group if you'd like to jam with us <3
# https://t.me/foxdot

b1 >> play("i", dur=2,sample=0,delay=[0,1/2,0,1/4],amplify=3/12,amp=1*var.brk1*(var.bld+var.drp))
b2 >> play("s",dur=1,delay=1/2,pan=(-2/3,2/3),amplify=P[2/3,1/2,3/4,4/5,4/5],amp=1*(var.brk1+var.drp))
b3 >> play("-",dur=1/4,sample=3,room=2/3,mix=1/3,pan=PWhite(-1,1),amplify=2/3,amp=1*var.brk)
b4 >> play("X",sample=1,dur=[1] * 8 + [0.5] * 8 + [0.25] * 4 + [0.125] * 2, drive=1/3,amp=1*(var.bld+var.drp))

b5 >> play("V",rate=P[[2, 1.8, 2, 2.1],[1.85, 1.5]] - var([0, 0.15], 8),delay=[var([0.25, 0], 64),[var([0, 0.5, 0.25, 0.5], 32), var([1/2, 1/4],128)],var([0, 0.25, 0, 0.5], [128, 64, 32, 32]),var([1/2,1/4],16)],amp=P[1, [0.75, 0.85]],lpf=var([500, 1500, 2000, 1000], 64), lpr=0.5) # groovy

kk >> play('X', rate=var([1, 0.95, 0.9], 128), sample=2, dur=1, lpf=500, lpr=0.75, amplify=P[1, 0.85] * var([1, 0], [128, 64, 256, [64, 128]]),amp=1*(var.bld+var.drp))

b6 >> play(".o",rate=6/5,lpf=expvar([1800, 5500], [128, 0]),amplify=5/3,amp=var([1, 0], [[64, 128], [32, [64, 32]]])) # <3

t1 >> creep(bassl,dur=1/2,sus=1/2,formant=2,hpf=linvar([800,1800],[32,0]),room=2/4,mix=1/2,amplify=linvar([2/5,3/5],8)*2,amp=1*(var.brk1+var.drp)).offbeat()
t2 >> dbass(bassl,oct=PRand([4,5]),dur=var([1/2,PDur(3,5)],[12,4]),sus=1/4,shape=1/4,lpf=([200,900],[32,0]),formant=linvar([0,1],[32,0]),pan=(-2/3,2/3),amplify=3/12,amp=1*(var.brk1+var.bld+var.drp))
t3 >> bell(notes,oct=(4,6),dur=4,sus=2,echo=1/2,room=2/3,mix=1/2,amplify=3/12,amp=1*(var.drp))
t4 >> ambi(notes,dur=4,chop=8,shape=2/7,formant=4,hpf=linvar([400,2000],64),room=1/3,mix=1/2,amplify=3/7,amp=1*(var.inout+var.brk1))
t5 >> arpy(bassl,oct=PRand([6,8]),dur=(1/4,PDur(3,5)),room=expvar([1/3,3/4],32),amplify=2/5,amp=1*(var.drp+var.bld))
t6 >> pluck(notes,oct=4,dur=PDur(3,5),sus=1/2,drive=1/8,formant=6,amplify=1/7, echo=0.2, echotime=0.2, mix=0.35, room=1,amp=1*(var.inout+var.brk1))

gT = Group(t1,t2,t3,t4,t5,t6)
gT.amp=0

#CTRL+ALT+ENTER 

## nest ##
#
#       .```.   _.''..
#      ;     ```      ``'.
#      :  0               `.
#      / >,:                \
#     /.'   `'.,             :
#    /'         ;.   .       ;
#   /          ;  \ ;     ; /
#              `..;\:     :'
#             __||   `...,'
#            `-,  )   ||
#             /.^/ ___||
#                 '---,_\       
#                    (/ `\
####

 
