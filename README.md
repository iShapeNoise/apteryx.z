
![](/Grafx/Apteryx_Icon/Apteryx.z_Icon.png)

## Apteryx(z)

<br>

* is a group of coding Artists, IP connectors and citizens of a global village, that gossip a newborn breed of audio-visual art.

* The choice of the group name has been inspired by a bird with the same name (also known as Kiwi) and it's funny head isolated movements that made us all laugh.

* This [dis]organized international collective of codejunk-spitting cyberpunks, a Global Conglomeration of independent musical artists jam, experiment, play, talk, and have fun while creating collaborative Art.

* Our thumb rule is:

>> "Let’s open up our editors, we will see and hear what is going to happen next."

<br>


People:

KittyClock (Prague, CZ)

BBScar (Berlin, DE)

Sprucer (S.F., USA)

Noodl (Detroit, USA)

Laelume (Brooklyn, USA)

![](/Grafx/UT_Bg_wBirds.png)
